package br.com.g3solutions.eproc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EventProcessor {
	
	private static final int PROCESS_ID_INDEX = 0;
	private static final int EVENT_DATE_INDEX = 1;
	private static final int EVENT_TYPE_INDEX = 2;
	private static final int EVENT_DETAIL_INDEX = 3;
	
	private static final DateTimeFormatter eventDateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	static void process(List<String> rawEvents) {
		
		List<Process> processes = parseRawEvents(rawEvents);
				
		processes = sortProcessesById(processes);
		
		processes.forEach(p -> p.removeUnimportantEvents());
		
		printOutput(processes);
	}

	private static List<Process> parseRawEvents(List<String> rawEvents) {

		//TODO

		for (String rawEvent : rawEvents) {
			String[] eventData = rawEvent.split(";");
			System.out.println("Process id:" + eventData[PROCESS_ID_INDEX]);
			System.out.println("Event date:" + LocalDate.parse(eventData[EVENT_DATE_INDEX], eventDateFormatter));
			System.out.println("Event type:" + EventTypes.byId(eventData[EVENT_TYPE_INDEX]).getDescription());
			System.out.println("Event detail:" + eventData[EVENT_DETAIL_INDEX]);
		}
		
		return new ArrayList<>();
	}

	private static List<Process> sortProcessesById(List<Process> processes) {
		//TODO
		return processes;
	}


	private static void printOutput(List<Process> processes) {
		StringBuilder output = new StringBuilder();

		for (Process process : processes) {
			
			output.append("Processo: " + process.getId());
			output.append("\n\n");
			
			for (Event event : process.getEvents()) {
				output.append("Data: " + eventDateFormatter.format(event.getDate()));
				output.append("Tipo: " + event.getType().getDescription());
				output.append("Detalhamento: " + event.getDetail());
				output.append("\n\n");
			}
			
			output.append("Qtd. eventos ignorados: " + process.getUnimportantEventsQuantity());
			output.append("\n\n");
		}
		
		System.out.println(output);
	}
}