package br.com.g3solutions.eproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Process {
	
	private Long id;
	
	private List<Event> events;
	
	private Integer unimportantEventsQuantity;
	
	Process(Long id){
		this.id = id;
		this.events = new ArrayList<>();
		this.unimportantEventsQuantity = 0;
	}
	
	Long getId() {
		return id;
	}
	
	public Integer getUnimportantEventsQuantity() {
		return unimportantEventsQuantity;
	}
	
	public List<Event> getEvents() {
		return Collections.unmodifiableList(events);
	}
	
	boolean addEvent(Event event) {
		return this.events.add(event);
	}
	
	void removeUnimportantEvents() {		
		for (Event event : events) {
			if(!event.getType().isImportant()) {
				events.remove(event);
				this.unimportantEventsQuantity++;
			}
		}
	}
}
