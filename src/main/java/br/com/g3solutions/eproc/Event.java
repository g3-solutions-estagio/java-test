package br.com.g3solutions.eproc;

import java.time.LocalDate;

class Event {

	private LocalDate date;

	private EventTypes type;

	private String detail;

	LocalDate getDate() {
		return date;
	}

	void setDate(LocalDate date) {
		this.date = date;
	}

	EventTypes getType() {
		return type;
	}

	void setType(EventTypes type) {
		this.type = type;
	}

	String getDetail() {
		return detail;
	}

	void setDetail(String detail) {
		this.detail = detail;
	}

}
