package br.com.g3solutions.eproc;

enum EventTypes {

	CONCILIATION_HEARING("AC", true, "Audiência de Conciliação"), 
	JUDGE_REVIEWING("JZ", false, "Sob Análise do Juiz"), 
	JURY_REVIEWING("JR", false, "Sob Análise do Júri"),
	FINAL_SENTENCE("S", true, "Sentença");

	EventTypes(String id, boolean important, String description) {
		this.id = id;
		this.important = important;
		this.description = description;
	}

	private String id;

	private boolean important;

	private String description;

	String getDescription() {
		return description;
	}
	
	boolean isImportant() {
		return important;
	}

	static EventTypes byId(String id) {
		for (EventTypes eventType : values()) {
			if (eventType.id.equals(id)) {
				return eventType;
			}
		}

		return null;
	}
}